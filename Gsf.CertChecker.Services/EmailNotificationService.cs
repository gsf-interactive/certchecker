using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;

namespace Gsf.CertChecker.Services
{
    public class EmailNotificationService
    {
        public async Task SendAsync(string to, string subject, string body)
        {
            string[] scopes = {GmailService.Scope.GmailSend};
            using var stream = new FileStream("credentials.json", FileMode.Open, FileAccess.Read);
            var credPath = "token.json";
            var credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                GoogleClientSecrets.Load(stream).Secrets,
                scopes,
                "user",
                CancellationToken.None,
                new FileDataStore(credPath, true)).Result;

            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Gsf.CertChecker"
            });

            string plainText = $"To: {to}{Environment.NewLine}" +
                               $"Subject: {subject}{Environment.NewLine}" +
                               $"Content-Type: text/html; charset=us-ascii{Environment.NewLine}{Environment.NewLine}" +
                               $"<p>{body}</p>";

            var message = new Message();
            message.Raw = Base64Encode(plainText);

            try
            {
                var x = await service.Users.Messages.Send(message, "me").ExecuteAsync();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
        }

        private string Base64Encode(string input)
        {
            var bytes = Encoding.UTF8.GetBytes(input);
            var base64 = Convert.ToBase64String(bytes)
                .Replace("+", "-")
                .Replace("/", "_")
                .Replace("=", "");

            return base64;
        }
    }
}