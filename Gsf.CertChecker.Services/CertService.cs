using System;
using System.Net;
using System.Threading.Tasks;

namespace Gsf.CertChecker.Services
{
    public class CertService
    {
        public async Task<DateTime> GetExpirationAsync(string url)
        {
            try
            {
                DateTime expirationDate = DateTime.MinValue;
                var request = (HttpWebRequest) WebRequest.Create($"https://{url}");

                request.AllowAutoRedirect = false;
                request.ServerCertificateValidationCallback = (sender, certificate, chain, policyErrors) =>
                {
                    DateTime.TryParse(certificate.GetExpirationDateString(), out expirationDate);
                    return true;
                };

                var response = await GetResponse(request);
                response.Close();

                return expirationDate;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error getting {url}", ex);
            }
        }

        private async Task<HttpWebResponse> GetResponse(HttpWebRequest request)
        {
            try
            {
                return (HttpWebResponse) await  request.GetResponseAsync();
            }
            catch (WebException we)
            {
                var response = we.Response as HttpWebResponse;
                if (response == null)
                    throw;

                return response;
            }
        }
    }
}