using System;

namespace Gsf.CertChecker.Services
{
    public class CertCheckResult
    {
        public string Url { get; set; }
        public DateTime Expiration { get; set; }
    }
}