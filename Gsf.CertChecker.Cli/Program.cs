﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading;
using System.Threading.Tasks;
using Gsf.CertChecker.Services;
using Kurukuru;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Gsf.CertChecker.Cli
{
    class Program
    {
        private static CertService _certService;
        private static Options _options;
        private static ConcurrentBag<CertCheckResult> _certCheckResults;
        private static int _completed;
        
        static int Main(string[] args)
        {
            try
            {
                MainAsync(args).Wait();
                return 0;
            }
            catch
            {
                return 1;
            }
            
        }

        static async Task MainAsync(string[] args)
        {
            _certCheckResults = new ConcurrentBag<CertCheckResult>();
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            try
            {
                _certService = new CertService();
                _completed = 0;

                await Spinner.StartAsync($"Checking {_completed} of {_options.Urls.Count} SSL Certificates...", async spinner =>
                {
                    var checkUrls = _options.Urls.Select(u => CheckExpirationAsync(u, spinner));
                    await Task.WhenAll(checkUrls);
                });

                if (_certCheckResults.Any())
                {
                    await Spinner.StartAsync($"Sending notification...", async spinner =>
                    {
                        var expirationMessages = _certCheckResults
                            .OrderBy(r => r.Expiration)
                            .Select(r => $"{r.Url} expires on {r.Expiration.ToShortDateString()}");
                    
                        var body = $"<p>Certs expiring in the next {_options.ExpirationWarningDays} days</p>" +
                                   string.Join("<br>", expirationMessages);
                    
                        await new EmailNotificationService().SendAsync(_options.EmailsToNotify, "Cert Report", body);
                        
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static async Task CheckExpirationAsync(string url, Spinner spinner)
        {
            var expiration = await _certService.GetExpirationAsync(url);

            if (expiration < DateTime.Now.AddDays(_options.ExpirationWarningDays))
            {
                _certCheckResults.Add(new CertCheckResult
                {
                    Expiration = expiration,
                    Url = url
                });
            }

            Interlocked.Increment(ref _completed);
            spinner.Text = $"Checking {_completed} of {_options.Urls.Count} SSL Certificates...";
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            // Configuration
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();
            _options = new Options();
            configuration.Bind(_options);
        }
    }
}