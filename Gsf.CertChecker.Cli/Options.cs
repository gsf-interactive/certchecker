using System.Collections.Generic;

namespace Gsf.CertChecker.Cli
{
    public class Options
    {
        public List<string> Urls { get; set; }
        public int ExpirationWarningDays { get; set; }
        public string EmailsToNotify { get; set; }
    }
}